## Help Message Strings

description = Pakethanterare för kommandoraden för hantering av modifikationer och tillägg till spel
merge-help = Installera eller ta bort paket
newuse-help =
    Inkluderar paket vars flaggor har ändrats sedan de senast
    installerades.
searchdesc-help = Inkludera beskrivningar vid sökning
# $atom (String) - The atom passed on the command line
not-installed = Inget paket som matchar { $atom } är installerat!
not-found = Inget paket som matchar { $atom } kunde hittas!
fetch-instructions = Hämta instruktioner för { $atom }:

## Query messages


## Package phase messages


## Module messages


## Dependency messages


# TODO: There are a number of context strings that may eventually be passed to DepError
# which should be internationalized


## Download messages


## Config messages


## News messages


## Flags messages


## Use flag messages


## Conflicts UI Messages


## Select messages


## Profile messages


## Use flag configuration messages


## VFS messages


## Loader messages


## Use string messages


## Questions


## Argparse generic


## Pybuild Messages


## Mirror Messages


## Repo Messages


## Init Messages


## Destroy Messages


## Prefix messages


## Locking Messages


## Validate Messages

