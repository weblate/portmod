## Help Message Strings

description = Gestionnaire de paquets en ligne de commande conçu pour empaqueter des mods de jeu
merge-help = Installe ou supprime un paquet
sync-help = Récupère et met à jour les dépôts de paquets distants
unmerge-help = Supprime le paquet donnée sans vérifier les dépendances.
no-confirm-help = Ne demande pas de confirmation et sélectionne toujours plutôt l'option par défaut.

## Query messages


## Package phase messages


## Module messages


## Dependency messages


# TODO: There are a number of context strings that may eventually be passed to DepError
# which should be internationalized


## Download messages


## Config messages


## News messages


## Flags messages


## Use flag messages


## Conflicts UI Messages


## Select messages


## Profile messages


## Use flag configuration messages


## VFS messages


## Loader messages


## Use string messages


## Questions


## Argparse generic


## Pybuild Messages


## Mirror Messages


## Repo Messages


## Init Messages


## Destroy Messages


## Prefix messages


## Locking Messages


## Validate Messages

